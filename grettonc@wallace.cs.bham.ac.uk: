<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
<HEAD>
	<META HTTP-EQUIV="CONTENT-TYPE" CONTENT="text/html; charset=utf-8">
	<TITLE>Charles Gretton's home page at University of Birmingham</TITLE>
	<META NAME="GENERATOR" CONTENT="OpenOffice.org 3.2  (Linux)">
	<META NAME="CREATED" CONTENT="0;0">
	<META NAME="CHANGEDBY" CONTENT="CogX User">
	<META NAME="CHANGED" CONTENT="20110720;7255900">
	<META NAME="Info 1" CONTENT="">
	<META NAME="Info 2" CONTENT="">
	<META NAME="Info 3" CONTENT="">
	<META NAME="Info 4" CONTENT="">
	<META NAME="CHANGEDBY" CONTENT="Charles Gretton">
	<META NAME="CHANGEDBY" CONTENT="Charles Gretton">
	<META NAME="CHANGEDBY" CONTENT="Charles Gretton">
	<META NAME="CHANGEDBY" CONTENT="Charles Gretton">
	<META NAME="CHANGEDBY" CONTENT="Charles Gretton">
	<META NAME="CHANGEDBY" CONTENT="Charles Gretton">
	<META NAME="CHANGEDBY" CONTENT="Charles Gretton">
	<META NAME="CHANGEDBY" CONTENT="Charles Gretton">
	<META NAME="CHANGEDBY" CONTENT="Charles Gretton">
	<META NAME="CHANGEDBY" CONTENT="Charles Gretton">
	<META NAME="CHANGEDBY" CONTENT="Charles Gretton">
	<META NAME="CHANGEDBY" CONTENT="Charles Gretton">
	<STYLE TYPE="text/css">
	<!--
		H3.ctl { font-family: "Lohit Hindi" }
		H2.ctl { font-family: "Lohit Hindi" }
	-->
	</STYLE>
</HEAD>
<BODY LANG="en-GB" DIR="LTR">
<H1>Charles Gretton (page valid from Nov 2008 – Aug 2011)</H1>
<P STYLE="margin-bottom: 0cm"><A HREF="http://www.cs.bham.ac.uk/~grettonc/me1.jpg"><IMG SRC="charlesInWater.jpg" NAME="HTTP://WWW.CS.BHAM.AC.UK/~GRETTONC/GRETTON.JPG" ALIGN=BOTTOM WIDTH=500 HEIGHT=375 BORDER=0></A>
</P>
<HR>
<H2 CLASS="western">About Me:</H2>
<P>In August 2011 I will have joined the Optimisation Group at NICTA,
based at the Canberra Research Lab. From November 2008 until August
2011 I was with the <A HREF="http://www.cs.bham.ac.uk/research/groupings/robotics_and_cognitive_architectures/">Intelligent
Robotics Lab</A> at the University of Birmingham. I also hold an
adjunct position with <A HREF="http://iiis.griffith.edu.au/">Griffith
University IIIS</A>. From May 2006 until November 2008 I was a
Researcher with <A HREF="http://www.griffith.edu.au/professional-page/abdul-sattar">Abdul
Sattar's</A> &quot;SAFE <A HREF="http://users.rsise.anu.edu.au/%7Echarlesg/247Posse.jpg">Agents</A>
Work Package&quot; based at the NICTA Brisbane Research Lab. Before
that I was a PhD candidate at the <A HREF="http://csl.anu.edu.au/">Computer
Sciences Laboratory</A> at the Australian National University.&nbsp; 
</P>
<H2 CLASS="western" STYLE="page-break-before: always">– <A HREF="http://www.cs.bham.ac.uk/~grettonc/publications.html">Papers</A>
/ <A HREF="http://www.cs.bham.ac.uk/~grettonc/publications.html">Publications</A>
– 
</H2>
<H2 CLASS="western">My primary interests</H2>
<P>Artificial intelligence:</P>
<UL>
	<LI><P>Planning – switching, decision-theoretic planning,
	relational generalisation, non-Markovian rewards and dynamics, and
	control knowledge</P>
	<LI><P>Machine Learning – Relational reinforcement learning,
	policy gradient, explanation based (analytical) learning</P>
	<LI><P>Knowledge Representation and Reasoning -- reasoning about
	actions, semantic web (ontology) and description logic</P>
	<LI><P>Search – bistate pruning, stochastic local search, anytime
	algorithms</P>
</UL>
<H2 CLASS="western">Contact:</H2>
<H3 CLASS="western">Address:</H3>
<P STYLE="margin-left: 1.06cm; margin-bottom: 0cm"><FONT FACE="Times New Roman"><B>Dr
Charles Gretton</B></FONT></P>
<P STYLE="margin-left: 1.06cm; margin-bottom: 0cm"><FONT FACE="Times New Roman"><B><A HREF="http://www.cs.bham.ac.uk/">School
of Computer Science</A><BR><A HREF="http://www.bham.ac.uk/">University
of Birmingham</A><BR>Edgbaston<BR>Birmingham<BR>B15 2TT</B></FONT></P>
<P STYLE="margin-bottom: 0cm"><BR>
</P>
<TABLE WIDTH=255 BORDER=0 CELLPADDING=2 CELLSPACING=0>
	<COL WIDTH=79>
	<COL WIDTH=168>
	<TR>
		<TD WIDTH=79>
			<P><B>E-mail:</B> 
			</P>
		</TD>
		<TD WIDTH=168>
			<P>c.gretton@cs.bham.ac.uk</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=79>
			<P><B>Telephone:</B></P>
		</TD>
		<TD WIDTH=168>
			<P>+44 121 41 58279</P>
		</TD>
	</TR>
	<TR>
		<TD WIDTH=79>
			<P><B>Fax:</B></P>
		</TD>
		<TD WIDTH=168>
			<P>+44 121 414 4281</P>
		</TD>
	</TR>
</TABLE>
<P STYLE="margin-bottom: 0cm"><BR>
</P>
<HR>
<H2 CLASS="western">Previous Students</H2>
<P>Dr Silvia Richter :: 2007-2011, PhD Degree Conferred on 6<SUP>th</SUP>
of May 2011:: <A HREF="http://nicta.com.au/people/richters/">webpage</A></P>
<H2 CLASS="western">Current Students</H2>
<P>Nathan Robinson :: 2007-2011:: &nbsp; <A HREF="http://nathanrobinson.info/">webpage</A></P>
<P STYLE="margin-bottom: 0cm"><BR>
</P>
<HR>
<H2 CLASS="western">Software</H2>
<UL>
	<LI><P><A NAME="Sequential_Multi-Core"></A>ay-Also-Plan –
	co-author with <A HREF="http://www.cs.bham.ac.uk/~ernitsj/">Juhan
	Ernits</A>. Runner up at the Sequential Multi-Core track of the
	International Planning Competition, 2011. This was a very surprising
	result for us, as the system was designed for massively parallel
	compute clusters, not a 4 CPU/ 8 core machine. The winning entry at
	IPC-2011 was <STRONG><SPAN STYLE="font-weight: normal">Arvand Herd
	by <A HREF="http://webdocs.cs.ualberta.ca/%7Enakhost/">Hootan
	Nakhost</A> et al.</SPAN></STRONG></P>
	<LI><P><A NAME="License"></A><A NAME="Parsing_Expression_Grammar_Template_Library"></A>
	c++-0x libraries I had written in 2008-09 for <A HREF="http://cogx.eu/">CogX</A>.
	<A HREF="http://www.cs.bham.ac.uk/~grettonc/CAST_RPC.tar.gz">CAST_RPC</A>
	makes writing procedural interactions between <A HREF="http://www.cs.bham.ac.uk/research/projects/cosy/cast/">CAST</A>
	(CoSy Architecture Schema Toolkit) components easy. And <A HREF="http://www.cs.bham.ac.uk/~grettonc/cassandra.tar.gz">cassandra</A>
	is a Parsing Expression Grammar based on <A HREF="http://www.colin-hirsch.net/">Colin
	Hirsch</A>'s <A HREF="http://code.google.com/p/pegtl/">pegtl</A>
	(Parsing Expression Grammar Template Library) for parsing <A HREF="http://www.cs.brown.edu/research/ai/pomdp/">POMDP</A>
	problem files. The latter now includes Finite-State Controller
	evaluation and improvement functionality.</P>
	<LI><P>gNovelty+-T and gNovelty-V2 (2009) – second author with
	<A HREF="http://nicta.com.au/people/phamdn">Duc-Nghia Pham</A>. 1<SUP>st</SUP>
	place in the random category threaded track (also the only entry),
	and 2<SUP>nd</SUP> place in the serial track.</P>
</UL>
<UL>
	<LI><P><A HREF="ipc6-nathan.pdf">CO-PLAN</A> (2008) -- co-author
	with Nathan Robinson and <A HREF="http://nicta.com.au/people/phamdn">Duc-Nghia
	Pham</A>. Combining SAT-Based Planning with Forward-Search</P>
</UL>
<UL>
	<LI><P><A HREF="ayPlan.tar.gz">ayPlan</A> (2008) -- Cost-optimal
	(also has a satisficing mode) state-based propositional planning
	(this is a module&nbsp;from CO-Plan)</P>
</UL>
<UL>
	<LI><P>Evacuator (2007) -- co-author with&nbsp;<A HREF="http://nicta.com.au/people/phamdn">Duc-Nghia
	Pham</A>. Simulates pedestrian evacuation from the Brisbane CBD
	(email me for details)</P>
</UL>
<UL>
	<LI><P>gNovelty+ -- second author with <A HREF="http://nicta.com.au/people/phamdn">Duc-Nghia
	Pham</A>. 1<SUP>st</SUP> place in the random category of the 2007
	SAT competition</P>
</UL>
<UL>
	<LI><P>RRL (2004 / 2005) -- see my ICAPS-07 paper</P>
</UL>
<UL>
	<LI><P>NMRDPP (2003) -- Non-Markovian Reward Decision Process
	Planner. 2<SUP>nd</SUP> place in the open and control knowledge
	tracks of the 2004 International Probabilistic Planning Competition</P>
</UL>
<UL>
	<LI><P>SyDRe (2002) -- second author with <A HREF="http://users.rsise.anu.edu.au/%7Ethiebaux/">Sylvie
	Thiébaux</A>. <A HREF="http://users.rsise.anu.edu.au/%7Ethiebaux/benchmarks/pds/">link</A></P>
</UL>
<HR>
<H2 CLASS="western">Teaching</H2>
<P>(2006; co-lecturer with Duc-Nghia Pham) &quot;Griffith -- 6206INT
-- Advanced Topics in Info Tech B&quot;</P>
<P>(2003; tutor) &quot;ANU -- comp2310 -- Concurrent and Distributed
Systems&quot;&nbsp;</P>
<HR>
<H2 CLASS="western">Appy polly loggy if this is all chepooka:</H2>
<P>Some interesting music (<A HREF="track_01.mp3">7/1998</A>, <A HREF="track_02.mp3">9/1998</A>)</P>
<P>My <A HREF="http://www.haroldgretton.com/">brother</A> has
released a CD of 20<SUP>th</SUP> century guitar music. Order it <A HREF="http://www.haroldgretton.com/cd/cd.html">here</A>.
</P>
<P><FONT COLOR="#010000">1939 -- 7,277 :: &lt;&lt;It was a period
when a literary panning, published in a review, could give you
something to<BR>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; think about for
thirty years.&gt;&gt;</FONT></P>
<P><FONT COLOR="#010000">&nbsp;&nbsp; -- from :: &quot;Sartre: The
Philosopher of the Twentieth Century'', Bernhard-Henri Lévy.</FONT></P>
<P><BR><BR>
</P>
<P><FONT COLOR="#010000"><A HREF="http://www.cs.bham.ac.uk/~grettonc/me2.jpg"><IMG SRC="charlesInWater2.jpg" NAME="HTTP://WWW.CS.BHAM.AC.UK/~GRETTONC/ME2.JPG" ALIGN=BOTTOM WIDTH=500 HEIGHT=375 BORDER=0></A></FONT></P>
<P>&nbsp;** Departing Monday 7 Jul 2305, and arriving Tuesday 8 July
:: Lufthansa LH779 Singapore to Frankfurt row 52 seat J. It was
raining in Wellington during takeoff. 
</P>
<P><A HREF="BhamIRLAB-Info.html">Locale...</A></P>
</BODY>
</HTML>