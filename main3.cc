#include<iostream>
#include<map>
#include<set>
#include<vector>
#include<sstream>
#include<cassert>

using namespace std;

typedef unsigned int uint;

#define RED 0;
#define BLUE 1;
#define GREEN 2;
#define UNKNOWN 3;


uint red = RED;
uint blue = BLUE;
uint green = GREEN;
uint unknown = UNKNOWN;

class Triangle
{
public:
    Triangle(uint c1, uint c2, uint c3, bool ordered = false)
        :ordered(ordered),
         sat1(false),
         sat2(false),
         sat3(false),
         sat4(false),
         sat5(false),
         sat6(false)
    {
        if(ordered){
            this->c1 = c1;
            this->c2 = c2;
            this->c3 = c3;

            return;
        }
        
        
        multiset<uint> nodes;
        nodes.insert(c1);
        nodes.insert(c2);
        nodes.insert(c3);
        
        multiset<uint>::const_iterator p = nodes.begin();
        
        this->c1 = *p;
        p++;
        this->c2 = *p;
        p++;
        this->c3 = *p;
        p++;
        assert(p == nodes.end());
        
    }
    uint c1, c2, c3;

    bool ordered;

    bool sat1;
    bool sat2;
    bool sat3;
    bool sat4;
    bool sat5;
    bool sat6;
    
    void satisfy(const Triangle& t)
    {
        bool notEverything = !(sat1 || sat2 || sat3 || sat4 || sat5 || sat6);
        
        assert(!ordered);
        
        assert(t.ordered);

        //cerr<<"Colors :: "<<c1<<" "<<c2<<" "<<c3<<endl;
        //cerr<<"Colors' :: "<<t.c1<<" "<<t.c2<<" "<<t.c3<<endl;
        
        Triangle t1(c1, c2, c3, true);
        Triangle t2(c1, c3, c2, true);
        Triangle t3(c2, c1, c3, true);
        Triangle t4(c2, c3, c1, true);
        Triangle t5(c3, c1, c2, true);
        Triangle t6(c3, c2, c1, true);
//         sat2 = true;
//         sat3 = true;
//         sat4 = true;
//         sat5 = true;
//         sat6 = true;
        

        if(t1 == t)
            sat1 = true;
        if(t2 == t)
            sat2 = true;
        if(t3 == t)
            sat3 = true;
        if(t4 == t)
            sat4 = true;
        if(t5 == t)
            sat5 = true;
        if(t6 == t)
            sat6 = true;

        assert(sat1 || sat2 || sat3 || sat4 || sat5 || sat6);

        if(notEverything && !(c1 == c2 && c2 == c3))
            assert(!(sat1 && sat2 && sat3 && sat4 && sat5 && sat6));
    }
    
    bool satisfied() const
    {
        assert(!ordered);
        
        return (sat1 && sat2 && sat3 && sat4 && sat5 && sat6);
        //return (sat1 || sat2 || sat3 || sat4 || sat5 || sat6);
    }
    
    
    bool operator==(const Triangle& t) const
    {
        return ((t.c1 == this->c1) && (t.c2 == this->c2) && (t.c3 == this->c3));
    }

    bool operator<(const Triangle& t) const
    {
        if(operator==(t))
            return false;

        if(t.c1 > this->c1)
            return true;

        if(t.c1 < this->c1)
            return false;

        if(t.c2 > this->c2)
            return true;

        if(t.c2 < this->c2)
            return false;
        
        if(t.c3 > this->c3)
            return true;

        if(t.c3 < this->c3)
            return false;
        
    }
    
};

class CompleteGraph
{
public:
    typedef pair<uint, uint> Edge;
    typedef pair<Edge, uint> EdgeColor;
    
    CompleteGraph(uint size)
        :MAXIMUM(size)
    {
        for(uint i = 0; i < MAXIMUM; i++){
            for(uint j = 0; j < MAXIMUM; j++){
                if(j != i){
                    coloring[Edge(i, j)] = unknown;

                    if(colours.find(i) == colours.end())
                        colours[i] = multiset<uint>();
                    
                    colours[i].insert(red);
                }
            }
        }
    }
    
    map<Edge, uint> coloring;
    map<uint, multiset<uint> > colours;
    uint MAXIMUM;

    /*This has to be true all the time.*/
    bool invariants()
    {
        for(uint i = 0; i < MAXIMUM; i++){
            if(coloring.find(Edge(i, i)) != coloring.end())
                return false;
        }

        
        for(uint i = 0; i < MAXIMUM; i++){
            for(uint j = 0; j < MAXIMUM; j++){
                if(i != j){
                    
                    if(coloring.find(Edge(i, j)) == coloring.end())
                        return false;
                    if(coloring.find(Edge(j, i)) == coloring.end())
                        return false;
                    if(coloring[Edge(i, j)] != coloring[Edge(j, i)])
                        return false;
                }
            }
        }
        
        return true;
    }


    vector<vector<Triangle> > allCombinations;
    
    void generatePermutations(const set<Triangle>& triangles, vector<vector<Triangle> >& allCombinations)
    {
        
    }
    
    
    bool addTriangles(uint node, const set<Triangle>& triangles)
    {
        
        if(allCombinations.size() == 0)
            generatePermutations(triangles, allCombinations);
        
        /*Generate every permuation of the argument triangles.*/
        vector<vector<Triangle> > allComb = allCombinations;
        
        for(vector<vector<Triangle> >::const_iterator i = allComb.begin()
            ; i != allComb.end()
            ; i++){

            bool failed = false;
            for(vector<Triangle>::const_iterator j = i->begin()
                ; j != i->end()
                ; j++){

                if(!addTriangle(node, *j)){
                    failed = true;
                    continue;
                }
            }

            if(!failed)
                return true;
            
        }
        return false;
    }

    void addTriangle(uint node, const Triangle& triangle)
    {
        for(uint i = 0; i < MAXIMUM; i++){
            if(i != node){
                for(uint j = 0; j < MAXIMUM; j++){
                    if(j != node && j != i){
                        /*HERE*/
                        triangle.c1;
                        triangle.c2;
                        triangle.c3;
                        
                    }
                }
            }
            
        }    
    }
    
    
    /*Like \function{isWinner} only with partial satisfaction.*/
    bool deadEnd(const set<Triangle>& triangles)
    {
        assert(invariants());
        
        for(uint node = 0; node < MAXIMUM; node++){
            if(!partialSatisfaction(node, triangles))
                return false;
        }
        return true;
    }
    

    /*For every noded, every uncolored edge can be colored.*/
    bool sanity(const set<Triangle>& triangles)
    {
        assert(invariants());
        
        
        for(uint node = 0; node < MAXIMUM; node++){
            bool canColor = false;
            for(uint color = 0; color < 3 && !canColor; color++){
                if(allEdgesColored(node) ||
                   allUnknownCanBeSat(color, node, triangles))
                    canColor = true;
            }
            if(!canColor){
                return false;
            }            
        }
        
        return true;
    }

    /*All the edges are colored.*/
    bool allEdgesColored(uint node)
    {
        assert(invariants());
        
        
        for(uint i = 0; i < MAXIMUM; i++){
            if(i != node){
                if(unknown == coloring[Edge(node, i)])
                    return false;
            }
        }
        return true;
    }
    
    /*All uncolored edges can be colored.*/
    bool allUnknownCanBeSat(const set<Triangle>& triangles,
                            uint node)
    {
        assert(invariants());
        
        return canAddColorAtNode(red, triangles, node) ||
            canAddColorAtNode(blue, triangles, node) ||
            canAddColorAtNode(green, triangles, node);
    }
    
    
    /*Can the argument color be placed at the argument node?*/
    bool canAddColorAtNode(uint color,
                           const set<Triangle>& triangles,
                           uint node)
    {
        assert(invariants());
        
        assert(!deadEnd());

        bool result = false;
        for(uint i = 0; i < MAXIMUM; i++){
            if(i != node){
                if(unknown == coloring[Edge(node, i)]){
                    coloring[Edge(node, i)] = color;
                    
                    if(deadEnd(triangles)){
                        coloring[Edge(node, i)] = unknown;
                    } else {
                        coloring[Edge(node, i)] = unknown;
                        result = true;
                        break;
                    }
                }
            }
        }
        
        return result;
    }

    bool partialSatisfaction(uint node, set<Triangle>& triangles)
    {
        assert(invariants());
        
       for(uint i = 0; i < MAXIMUM; i++){
            if(i != node){
                for(uint j = 0; j < MAXIMUM; j++){
                    if(i != j && j != node){

                        assert(coloring.find(Edge(node, i)) != coloring.end());
                        assert(coloring.find(Edge(i, j)) != coloring.end());
                        assert(coloring.find(Edge(j, node)) != coloring.end());
                        
                        Triangle t(coloring[Edge(node, i)], coloring[Edge(i, j)], coloring[Edge(j, node)]);
                        if(triangles.find(t) != triangles.end()){
                            ;//unsat.erase(t);
                        } else {
                            return false;
                        }
                    }
                }
            }
        }
       return true;
    }
    
    
    bool satisfies(uint node, set<Triangle>& triangles)
    {
        assert(invariants());
        
        
        set<Triangle> unsat = triangles;
        for(uint i = 0; i < MAXIMUM; i++){
            if(i != node){
                for(uint j = 0; j < MAXIMUM; j++){
                    if(i != j && j != node){

                        assert(coloring.find(Edge(node, i)) != coloring.end());
                        assert(coloring.find(Edge(i, j)) != coloring.end());
                        assert(coloring.find(Edge(j, node)) != coloring.end());
                        
                        Triangle t(coloring[Edge(node, i)], coloring[Edge(i, j)], coloring[Edge(j, node)]);
                        if(triangles.find(t) != triangles.end()){
                            unsat.erase(t);
                        } else {
                            return false;
                        }
                    }
                }
            }
        }

        if(unsat.size() != 0)return false;
        else return true;
    }

    /*Check that each node satisfies the constraint.*/
    bool isWinner(set<Triangle>& triangles)
    {
        assert(invariants());
        
        for(uint node = 0; node < MAXIMUM; node++){
            if(!satisfies(node, triangles))
                return false;
        }
        return true;
    }
};



#define Maximus(I, J, COL) {\
    assert(I != J); \
    uint col = cg.coloring[CompleteGraph::Edge(I, J)]; \
    cg.coloring[CompleteGraph::Edge(I, J)] = COL; \
    cg.coloring[CompleteGraph::Edge(J, I)] = COL; \
    cg.colours[I].erase(cg.colours[I].find(col)); \
    cg.colours[J].erase(cg.colours[J].find(col)); \
    cg.colours[I].insert(COL); \
    cg.colours[J].insert(COL); \
}\


/*This time I am only going to try and colour graphs with the missing
 *triangles.*/
int main(int argc, char** argv)
{
    set<Triangle> triangles;
    /*Hard set.*/
    Triangle t1(red, red, blue);
    Triangle t2(red, red, green);
    Triangle t3(green, green, red);
    Triangle t4(blue, blue, red);
    Triangle t5(red, red, red);
    Triangle t6(blue, blue, blue);
    Triangle t7(blue, green, red);
    triangles.insert(t1);
    triangles.insert(t2);
    triangles.insert(t3);
    triangles.insert(t4);
    triangles.insert(t5);
    triangles.insert(t6);
    triangles.insert(t7);


//     Triangle t4(blue, blue, red);
//     Triangle t5(red, red, blue);
//     triangles.insert(t5);
//     triangles.insert(t4);
    
    
//     Triangle t1(red, red, blue);
//     Triangle t2(blue, blue, green);
//     Triangle t3(green, green, red);
//     Triangle t4(green, green, green);
//     Triangle t5(red, red, red);
//     Triangle t6(blue, blue, blue);
//     triangles.insert(t1);
//     triangles.insert(t2);
//     triangles.insert(t3);
//     triangles.insert(t4);
//     triangles.insert(t5);
//     triangles.insert(t6);

    
    
//     Triangle t1(red, red, red);
//     Triangle t5(red, red, blue);
//     triangles.insert(t1);
//     triangles.insert(t5);

    
//     Triangle t2(blue, blue, blue);
//     Triangle t5(red, red, blue);
//     triangles.insert(t2);
//     triangles.insert(t5);

    /*SPECIAL 6, 7, 8*/
//     Triangle t1(red, red, red);
//     Triangle t5(red, red, blue);
//     Triangle t4(blue, blue, red);
//     triangles.insert(t1);
//     triangles.insert(t5);
//     triangles.insert(t4);


    /* ** ORIGINAL ** */
//     Triangle t1(red, red, red);
//     Triangle t5(red, red, blue);
//     Triangle t4(blue, blue, red);
//     Triangle t6(blue, blue, blue);
//     triangles.insert(t1);
//     triangles.insert(t5);
//     triangles.insert(t4);
//     triangles.insert(t6);


//     Triangle t1(red, red, red);
//     Triangle t2(blue, blue, blue);
//     Triangle t5(red, red, blue);
//     triangles.insert(t1);
//     triangles.insert(t2);
//     triangles.insert(t5);



    
    
//     Triangle t1(red, red, red);
//     Triangle t2(blue, blue, blue);
//     Triangle t4(blue, blue, red);
//     Triangle t5(red, red, blue);
    
//     triangles.insert(t1);
//     triangles.insert(t2);
//     triangles.insert(t4);
//     triangles.insert(t5);

    
//     Triangle t5(red, red, blue);
//     Triangle t2(blue, blue, blue);
//     Triangle t7(blue, green, red);
    
// //     Triangle t8(blue, green, red);
// //     Triangle t9(red, blue, green);
// //     Triangle t10(green, red, blue);

    
// //     triangles.insert(t8);
// //     triangles.insert(t9);
// //     triangles.insert(t10);
    
// //     assert(triangles.size() == 7);

// //     exit(0);
    
    /*Semi soft*/

    /*Very soft*/
//     triangles.insert(t2);
//     triangles.insert(t5);
//     triangles.insert(t7);

    //assert(triangles.size() == 3);

    /*Counting the number of complete graphs we have.*/
    vector<map<CompleteGraph::Edge, uint> > allModels;
    
    string str(argv[1]);
    istringstream oss(str);
    uint size;
    oss>>size;

    bool solved = false;
    for(; !solved; size++){

        cerr<<"Going for number :: "<<size<<endl;
        //CompleteGraph cg(size);
        CompleteGraph cg(size);
    
        set<map<CompleteGraph::Edge, uint> > coloringsSofar;
        bool change;// = false;

        /*Map from nodes to the triangles that have been satisfied at that node.*/
        map<uint, set<Triangle> > included;

        Triangle allRed(red, red, red, true);
    
        for(uint i = 0; i < size; i++){
            included[i] = set<Triangle>();
            included[i].insert(allRed);
        }
    
    
        vector<Triangle> vTriangles(triangles.begin(), triangles.end());
        unsigned long int count = 0;
        while(count < 300000){
            //count ++;
        
            uint i  = (uint) (size*1.0*rand()/(RAND_MAX+1.0));
            uint j  = (uint) (size*1.0*rand()/(RAND_MAX+1.0));
            uint k  = (uint) (size*1.0*rand()/(RAND_MAX+1.0));
            uint col  = (uint) (3*1.0*rand()/(RAND_MAX+1.0));

            /*We are about to change a triangle if this condition doesn't hold.*/
            if(i == j || i == k || j == k)
                continue;

            uint c1;
            uint c2;
            uint c3;
            bool notDone = true;
            bool broken = false;

            uint count2 = 0;
            while(notDone){
                count2 ++;

                //cerr<<".";
            
                uint tr  = (uint) (vTriangles.size()*1.0*rand()/(RAND_MAX+1.0));
                Triangle& tri = vTriangles[tr];

                uint prob  = (uint) (100*1.0*rand()/(RAND_MAX+1.0));

                /*Generate a particular coloring.*/
                if(prob < 33){
                    prob  = (uint) (100*1.0*rand()/(RAND_MAX+1.0));
                    c1 = tri.c1;
                    if(prob < 50){
                        c2 = tri.c2;
                        c3 = tri.c3;
                    } else {
                        c2 = tri.c3;
                        c3 = tri.c2;
                    }
                } else if ( prob < 66) {
                    prob  = (uint) (100*1.0*rand()/(RAND_MAX+1.0));
                    c1 = tri.c2;
                    if(prob < 50){
                        c2 = tri.c1;
                        c3 = tri.c3;
                    } else {
                        c2 = tri.c3;
                        c3 = tri.c1;
                    }
                } else {
                    prob  = (uint) (100*1.0*rand()/(RAND_MAX+1.0));
                    c1 = tri.c3;
                    if(prob < 50){
                        c2 = tri.c1;
                        c3 = tri.c2;
                    } else {
                        c2 = tri.c2;
                        c3 = tri.c1;
                    }
                }

                assert(c1 == red || c1 == blue || c1 == green);
                assert(c2 == red || c2 == blue || c2 == green);
                assert(c3 == red || c3 == blue || c3 == green);
            
                Triangle tmp(c1, c2, c3, true);

                /*If we actually need the triangle we have found.*/
                if(included[i].find(tmp) == included[i].end()){
                    notDone = false;
                    included[i].insert(tmp);
                } else if(count2 > 42*3){
                    prob  = (uint) (100*1.0*rand()/(RAND_MAX+1.0));
                    if(prob < 90){    
                        broken = true;
                        notDone = false;
                    } else {
                        //cerr<<"*Looser.\n";
                    
                        notDone = false;

                        uint tc1 = cg.coloring[CompleteGraph::Edge(i, j)];
                        uint tc2 = cg.coloring[CompleteGraph::Edge(j, k)];
                        uint tc3 = cg.coloring[CompleteGraph::Edge(k, i)];
                    
                        Triangle tmp(tc1, tc2,tc3, true);

                        //assert(included[i].find(tmp) != included[i].end());
                    
                        included[i].erase(tmp);
                    }  
                }
            }

            if(broken)
                continue;
        
            //cerr<<col<<endl;
        
            Maximus(i, j, c1);
            Maximus(j, k, c2);
            Maximus(k, i, c3);


            count ++;
            if(!(count % 10000))
                cerr<<count<<endl;
        
            bool result = false;
            if(count >= 0){  
                //cerr<<"test."<<endl; 
                result = cg.isWinner(triangles);
            } else {
                //cerr<<".";
            }
        
        
            if(result){
                solved = true;
                break;

            
                uint prevSize = coloringsSofar.size();
                coloringsSofar.insert(cg.coloring);

                if(prevSize == coloringsSofar.size()){
                    char ch;
                    cerr<<"***********WOOR WOOT WOOR!!!!!!!!!!!!!!!!!!!!!!!!!\n";
                    cin>>ch;
                }
            
                cerr<<"Number of models :: "<<coloringsSofar.size()<<endl;//" "<<coloringsSofar.size()<<endl;
                cg = CompleteGraph(size);
            
                for(uint i = 0; i < size; i++){
                    included[i] = set<Triangle>();
                    included[i].insert(allRed);
                }
            
                //break;
            }
        }

        cerr<<"Got in in count :: "<<count<<endl;

//         for(uint i = 0; i < size; i++ ){   
//             for(uint j = i + 1; j < size; j++ ){
//                 uint color = cg.coloring[CompleteGraph::Edge(i, j)];
//                 cerr<<"["<<i<<", "<<j<<"] = "<<color<<";\n";
//             }
//         }
    }
    
    
    return 0;
}


